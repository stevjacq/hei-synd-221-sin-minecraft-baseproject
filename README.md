# Minecraft Base Project
# Introduction

Dans ce projet nous devons faire la communication entre un programme en java et le jeu vidéo minecraft. Le programme permet d'acquérir les différentes valeurs de production et de consommation pour les afficher sur une page web et pour également les modifier depuis cette dernière. Nous stockons ces valeur dans une base de données qui s'appelle InfluxDB et nous pouvons les visualiser grâce à Grafana.

# Function
## Modbus

Notre programme accède à un modbus serveur présent dans le jeu. Ce dernier permet d'accéder à toutes les valeurs dont nous avons besoin. Pour cela nous utilisons la librairie modbus4j. Les différents registres sont listés dans un fichier .csv lequel est utilisé pour créer les différentes variables. Ensuite ces valeurs sont stockées dans des DataPoints.

## Database

Pour stocker les données et garder un historique ce qu'il s'est passé, chaque valeurs est envoyée à InfluxDB database. Pour visualiser ces valeurs nous utilisons Grafana qui nous permet également de manipuler la base de donnée.

## Web

Notre projet contient un "websocket" qui nous permet d'accéder aux valeurs en temps réel depuis une page web. Nous pouvons également modifier des valeurs comme par exemple la puissance de l'usine à charbon.

## .jar

Le projet contiens un .jar qui peut être utiliser pour tester. Ce dernier utilise grafana.hevs.sdi.ch server et se connecte sur le minecraft world présent sur votre ordinateur.

Paramètres: db_url db_name db_user db_password modbus_host modbus_port

java -jar Minecraft.jar https://influx.sdi.hevs.ch SIn09 SIn09 KQepbOafV3CcthVtjr5P9Cj_QrNkGMXPGeMRssrs5hkTOjk-86ApG-roKqBK3xZpYwupZcqPlQLXjfj1po5tFw== localhost 1502

Vous pouvez avoir le .jar en téléchargeant [jarlink](https://gitlab.com/stevjacq/hei-synd-221-sin-minecraft-baseproject/-/tree/master/out/artifacts/Minecraft_jar)

## javadoc

Toute la javadoc est disponible [link](https://gitlab.com/stevjacq/hei-synd-221-sin-minecraft-baseproject/-/tree/master/javadoc/hei)


IntelliJ base project related to Minecraft Lab of 221_SIn & 231_SIn course