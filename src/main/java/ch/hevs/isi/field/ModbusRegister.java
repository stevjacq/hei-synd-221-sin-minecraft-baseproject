package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief c'est une classe abstraire qui représente un registre Modbus

 */
public abstract class ModbusRegister {
    protected int address;

    protected DataPoint dataPoint = null;
    /**
     * relie les datapoints au modbusregister
     */
    protected static Map<DataPoint, ModbusRegister> registerMap = new HashMap<>();


    /**
     * cette méthode prend un datapoint en parmètre et retourne le modbusregister correspondant dans le registerMap
     * @param dp
     * @return
     */
    public static ModbusRegister getRegisterFromDataPoint(DataPoint dp) {
        return registerMap.get(dp);
    }

    /**
     * implémenté dans les sous classes
     */
    public abstract void read();

    /**
     * implémenté dans les sous classes
     */
    public abstract void write();

    /**
     * La méthode  prend un entier "millis" en paramètre qui spécifie le temps entre chaque lecture de registre.
     * Elle utilise un objet "Timer" pour lancer une tâche de lecture périodique.
     * @param millis
     */
    public static void startPolling(int millis) {
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new PollTask(), 0, millis);


    }

    /**
     *  la méthode boucle sur tous les "ModbusRegisters" du "registerMap"
     *  et appelle la méthode "read()" si le "DataPoint" correspondant n'est pas une sortie
     */
    public static void poll() {
        for (ModbusRegister mr : registerMap.values()) {
            if (!mr.dataPoint.isOutput()) {
                mr.read();
            }
        }


    }
}
