package ch.hevs.isi.field;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe implémente l'interface DataPointListener
 * c'est un "singletons" car  elle a une seule instance
 */
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;

import java.util.Timer;

public class FieldConnector implements DataPointListener {

    private static FieldConnector instance;

    /**
     cette methode est appelée lorsqu'un "DataPoint" est enregistré.
     Cette méthode utilise un objet "ModbusRegister" pour écrire les données du "DataPoint".
     * @param dp
     */
    @Override
    public void onNewValue(DataPoint dp) {
        // pushToField(dp.getLabel(), dp.toString());

        ModbusRegister mr = ModbusRegister.getRegisterFromDataPoint(dp);

        mr.write();

    }

    /**
     * Cette focntion permet de créer une instance si il'ny en a pas encore une
     * @return l'instance
     */
    public static FieldConnector getInstance() {
        if (instance == null) {
            instance = new FieldConnector();

        }
        return instance;

    }

    /**
     * Cette methode permet de lancer la lecture des données
     */
    private FieldConnector() {
        ModbusRegister.startPolling(5000);
    }





}
