package ch.hevs.isi.field;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief La classe "FloatRegister" étend la classe "ModbusRegister". Elle utilise un objet "ModbusAccessor"
 * pour lire et écrire des valeurs float à une adresse donnée.
 **/
import ch.hevs.isi.core.FloatDataPoint;

public class FloatRegister extends ModbusRegister {

    private static ModbusAccessor ma = ModbusAccessor.getInstance();
    private int offset;
    private int range;

    /**
     *  ce constructeur prend en entrée plusieurs paramètres tels que le label, l'adresse,
     *  l'étendue et le décalage pour créer un nouvel objet "FloatDataPoint" et le mapper à l'objet "FloatRegister"
     * @param label
     * @param isOutput
     * @param address
     * @param range
     * @param offset
     */
    public FloatRegister(String label, boolean isOutput, int address, int range, int offset) {
        dataPoint = new FloatDataPoint(label, isOutput);
        this.address = address;
        this.range = range;
        this.offset = offset;
        registerMap.put(dataPoint, this);
    }

    /**
     * Lorsque la fonction est appelée, le champ de valeur dans l'objet DataPoint correspondant est mis à jour
     *
     * @detail elle  lit une valeur float à l'adresse donnée en utilisant l'objet "ModbusAccessor".
     * Ensuite, elle multiplie cette valeur par le range et ajoute l'offset pour obtenir la valeur réelle.
     * Enfin, la méthode met à jour la valeur de l'objet "dataPoint" avec la valeur réelle.
     */
    public void read() {
        float value = ma.readFloat(address) * range + offset;

        dataPoint.setValue(value);

    }
    /**
     * Lorsque cette fonction est appelée, le champ de valeur actuel dans
     * le point de données correspondant est écrit le registre Modbus
     *( l'attribut value du DataPoint est écrit dans le registre)
     *
     * @detail
     * Pour ce faire, la méthode calcule d'abord la valeur normalisée (sans l'offset et le range)
     * en utilisant l'attribut "value" de l'objet "dataPoint", puis la divise par le range et ajoute l'offset.
     * Ensuite, la méthode écrit cette valeur normalisée dans le registre en utilisant l'objet "ModbusAccessor".
     *
     */
    public void write() {
        Float value = ((float)dataPoint.getValue() - offset) / range;
        ma.writeFloat(address, value);

    }
}
