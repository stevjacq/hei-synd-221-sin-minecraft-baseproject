package ch.hevs.isi.field;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe permet de creer  FloatRegister et BooleanRegister à partir de ModbusMap csv
 */
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;

public  class FieldConfigurator {

    /**
     *cette méthode lit les informations de configuration d'un fichier CSV et crée des objets FloatRegister
     * ou BooleanRegister en fonction de ces informations.
     * Le fichier CSV est analysé en utilisant la méthode fileParser de la classe Utility pour lire les lignes du fichier.
     * Chaque ligne est ensuite traitée pour extraire les informations nécessaires à la création des objets FloatRegister
     * ou BooleanRegister. Si la ligne commence par "Label",
     * elle est ignorée car elle contient simplement des en-têtes de colonnes. Les informations sur l'étiquette,
     * le type (float ou booléen), la direction (entrée ou sortie), l'adresse,
     * la plage et l'offset sont lues à partir des colonnes appropriées. Ensuite,
     * un nouvel objet FloatRegister ou BooleanRegister est créé en utilisant ces informations.
     * @param filename
     */
    public static void fromCSV(String filename) {
        BufferedReader br = Utility.fileParser(null, filename);
        if (br != null) {
            try {
                String line = br.readLine();

                while (line != null) {

                    if (!line.startsWith("Label")) {
                        String config[] = line.split(";");

                        // Decode csv file
                        String label = config[0];
                        boolean isFloat = config[1].equals("F");
                        boolean isOutput = config[3].equals("Y");
                        int address = Integer.parseInt(config[4]);
                        int range= Integer.parseInt(config[5]);
                        int offset= Integer.parseInt(config[6]);
                        if (!label.equalsIgnoreCase("label")) {
                            if (isFloat) {
                                new FloatRegister(label, isOutput, address, range, offset);
                            } else {
                                new BooleanRegister(label, isOutput, address);
                            }
                        }
                    }

                    line = br.readLine();
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void getFile(String s) {
    }
}
