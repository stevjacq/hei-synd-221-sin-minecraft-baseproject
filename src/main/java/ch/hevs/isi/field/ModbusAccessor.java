package ch.hevs.isi.field;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Comprend la bibliothèque Modbus4j Modbus
 * cette classe ModbusAccessor doit permettre la lecture et l’écriture de valeurs float et boolean
 * dans des registres sélectionnés
 * c'est un "singleton" car  elle a une seule instance

 **/
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;


public class ModbusAccessor {
    private String host;
    private int port;
    private byte unit;
    private static ModbusAccessor instance = null;
    private ModbusMaster ma;

    private ModbusAccessor() {
    }

    /**
     * cette focntion permet de créer une instance s'il n'y en existe pas deja une et la retourner
     * @return instance
     */
    public static ModbusAccessor getInstance() {
        if (instance == null) {
            instance = new ModbusAccessor();

        }
        return instance;

    }

    /**
     * cette focntion permet de se connecter au modbus
     * @param host
     * @param port
     * @param unit
     * @return si la connexion est établie
     */
    public boolean connect(String host, int port, byte unit) {

        IpParameters ip = new IpParameters();
        this.host = host;
        this.unit = unit;
        this.port = port;

        ip.setHost(host);
        ip.setPort(port);
        ma = new ModbusFactory().createTcpMaster(ip, true);
        try {
            ma.init();
            return true;
        } catch (ModbusInitException e) {
            return false;
        }


    }

    /**
     * cette focntion permet de lire la valeur de notre objet
     * @return boolean
     */
    public Boolean readBoolean(int register) {

        try {
            return ma.getValue(BaseLocator.coilStatus(unit, register));
        } catch (ModbusTransportException e) {
            //throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            //throw new RuntimeException(e);
        }
        return null;
    }

    /**
     * cette focntion permet de lire la valeur de notre registre
     * @param register
     * @return float
     */
    public Float readFloat(int register) {
        try {
            return (float) ma.getValue(BaseLocator.inputRegister(unit, register, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException e) {
            // throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            //throw new RuntimeException(e);
        }
        return null;
    }

    /**
     * cette fonction permet de seter la valeur dans le registre en float
     * @param register
     * @param value
     */
    public void writeFloat(int register, float value) {
        try {

            ma.setValue(BaseLocator.holdingRegister(unit, register, DataType.FOUR_BYTE_FLOAT), value);
        } catch (ModbusTransportException e) {
            // throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            //throw new RuntimeException(e);
        }

    }

    /**
     * cette fonction permet de seter la valeur dans le registre en boolean
     * @param register
     * @param value
     */
    public void writeBoolean(int register, boolean value) {
        try {

            ma.setValue(BaseLocator.coilStatus(unit, register), value);
        } catch (ModbusTransportException e) {
            // throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            //throw new RuntimeException(e);
        }

    }

    /**
     * permet de tester les methode de la classe
     * @param args
     */
    public static void main(String[] args) {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        if (ma.connect("localhost", 1502, (byte) 1)) {
            System.out.println("youhou");

            Boolean wind = ma.readBoolean(609);
            if (wind != null)
                System.out.println("Wind turbine ON ? " + (wind ? "YES" : "NO"));
        } else
            System.err.println("argh");

        ma.writeBoolean(401, true);
        ma.writeFloat(209, 0f);

        System.out.println(ma.readFloat(209));
    }
}
