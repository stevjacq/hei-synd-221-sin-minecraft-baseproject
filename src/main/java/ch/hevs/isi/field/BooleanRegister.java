package ch.hevs.isi.field;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe est une héritiaire de la classe ModbusRegister
 */
import ch.hevs.isi.core.BooleanDataPoint;

public class BooleanRegister extends ModbusRegister {

    private static ModbusAccessor ma = ModbusAccessor.getInstance();

    /**
     *ce constructeur permettant de créer un objet BooleanRegister avec un label,
     * une valeur booléenne indiquant s'il s'agit d'une sortie ou non, et une adresse.
     * @param label
     * @param isOutput
     * @param address
     */
    public BooleanRegister(String label, boolean isOutput, int address) {
        dataPoint = new BooleanDataPoint(label, isOutput);
        this.address = address;
        registerMap.put(dataPoint, this);
    }

    /**
     * Lorsque la fonction est appelée, le champ de valeur dans l'objet
     * DataPoint correspondant est mis à jour
     */
    public void read() {
        boolean value = ma.readBoolean(address);

        dataPoint.setValue(value);

    }

    /**
     *Lorsque cette fonction est appelée, le champ de valeur actuel dans
     * le point de données correspondant est écrit en Modbus
     *( l'attribut value du DataPoint est écrit dans le registre)
     */
    public void write() {
       Boolean value = (Boolean)  dataPoint.getValue();
        ma.writeBoolean(address, value);

    }


}
