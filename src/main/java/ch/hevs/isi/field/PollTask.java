package ch.hevs.isi.field;

import java.util.TimerTask;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe représente une tâche périodique lancée par un objet "Timer"
 * cette classe permet d'effectuer une lecture périodique des registres Modbus pour les "DataPoints" correspondants,
 * en utilisant la méthode "startPolling()" de la classe "ModbusRegister".

 */
public class PollTask extends TimerTask{
    @Override
    /**
     * la méthode est appelée à chaque intervalle de temps spécifié, et elle appelle la méthode "poll()" de la classe
     * "ModbusRegister", qui à son tour boucle sur tous les "ModbusRegisters" du "registerMap" et
     * appelle la méthode "read()" si le "DataPoint" correspondant n'est pas une sortie.
     */
    public void run() {
        ModbusRegister.poll();
    }
}
