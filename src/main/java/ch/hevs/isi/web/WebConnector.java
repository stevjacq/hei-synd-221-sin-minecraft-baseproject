package ch.hevs.isi.web;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief elle implémente l'interface "DataPointListener" et se charge de gérer les connexions WebSocket
 * pour envoyer des données à un client. Elle contient une instance unique ("instance") et
 * un objet de type "WebSocketServer" ("wss").
 *
 * @detail Pour mettre à jour une sortie, une page Web transmet le ''label'' et la nouvelle "value" du
 * point de données correspondant
 * Comme une page Web fait référence à un DataPoint uniquement par son "label",
 * le WebConnector a besoin de trouver une référence au "DataPoint" pour accéder à sa valeur.
 * la "map" permet cette opération
 * cette classe  doit transformer en un serveur web socket acceptant les connexions parallèles de plusieurs pages du navigateur web sur le port 8888
 * Toutes les pages Web ouvertes doivent afficher:
 * la valeur correcte pour les entrées : tout changement effectué dans le champ doit être répercuté
 * dans tous les navigateurs Web, et
 * la bonne valeur des sorties : toute modification effectuée par un navigateur web doit être
 * modifié dans tous les autres navigateurs Web
 * L'état des sorties doit s'afficher correctement lorsque la page Web est chargée
 */
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.ServerSocket;

public class WebConnector implements DataPointListener {

    private static WebConnector instance = null;
    private WebSocketServer wss=null;
    @Override
    /**
     *cette méthode envoie les données du DataPoint à la méthode "pushToWeb()"
     * qui les transmets aux clients connectés.
     */
    public void onNewValue(DataPoint dp) {
        pushToWeb(dp.getLabel(), dp.toString());
    }

    /**
     *elle permet de récupérer l'instance unique de "WebConnector". Si l'instance n'a pas encore été crée,
     * elle est crée dans le constructeur.
     * @return instance
     */
    public static WebConnector getInstance() {
        if (instance == null) {
            instance = new WebConnector();

        }
        return instance;

    }

    /**
     *Le constructeur initialise un serveur WebSocket à l'adresse "8888".
     * Lorsqu'une connexion WebSocket est ouverte, le serveur envoie un message de bienvenue à l'utilisateur connecté.
     * Lorsqu'un message est reçu, il est analysé pour extraire le label et la valeur du point de données,
     * puis la valeur est convertie en boolean ou float
     */
    private WebConnector(){
        wss=new WebSocketServer(new InetSocketAddress(8888)) {
            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                webSocket.send("Welcome Steven");
            }

            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {

            }

            @Override
            public void onMessage(WebSocket webSocket, String s) {
                // Message s: "<label>=<value>;

                String d []= s.split("=");
                String value = d[1];
                DataPoint dp = DataPoint.getDataPointFromLabel(d[0]);

                if (value.equalsIgnoreCase("true"))
                    dp.setValue(true);
                else if (value.equalsIgnoreCase("false"))
                    dp.setValue(false);
                else
                    dp.setValue(Float.parseFloat(value));
            }

            @Override
            public void onError(WebSocket webSocket, Exception e) {

            }

            @Override
            public void onStart() {

            }
        };
        wss.start();


    }

    /**
     *La méthode "pushToWeb()" itère sur toutes les connexions WebSocket actives
     * et envoie les données de chaque point de données à tous les clients connectés.
     * @param label
     * @param value
     */
    private void pushToWeb(String label, String value) {
        System.out.println("pushToWeb(" + label + ", " + value + ")");
        for (WebSocket ws: wss.getConnections()) {
            ws.send(label +"=" + value);
        }
    }

    /**
     * utilisé pour tester les methode
     * @param args
     */
    public static void main(String[] args) {
       // WebConnector.getInstance();
    }
}
