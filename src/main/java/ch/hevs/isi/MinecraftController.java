package ch.hevs.isi;

import ch.hevs.isi.SmartControl.Controller;
import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConfigurator;
import ch.hevs.isi.field.ModbusAccessor;
import ch.hevs.isi.field.ModbusRegister;
import ch.hevs.isi.web.WebConnector;

/**
 * @author elise Zanou, steven Jacquier, Patrice Rudaz
 * @date 21/04/2023
 * @brief Le code définit d'abord les valeurs par défaut des paramètres de connexion InfluxDB et ModbusTCP,
 * qui sont ensuite écrasées si l'utilisateur les fournit en tant qu'arguments de ligne de commande.
 * Le tableau `parameters` est initialisé à `null`,
 * puis défini sur les arguments de la ligne de commande s'il y en a plus de cinq.
 * Les arguments sont analysés et utilisés pour définir les valeurs appropriées pour les paramètres InfluxDB et ModbusTCP.
 * La variable `ERASE_PREVIOUS_DATA_INB_DB` est définie sur `true` si l'argument de ligne de commande `"-erasedb"` est présent.
 */
public class MinecraftController {

    public static boolean ERASE_PREVIOUS_DATA_INB_DB = false;


    public static void usage() {
        System.out.println();
        System.out.println("You're reading this message because no parameter (or not the needed ones) has been passed to the application.");
        System.out.println();
        System.out.println("In development mode, just add to your running configuration the needed parameters (see usage below).");
        System.out.println("In running mode, the application's usage is the following:");
        System.out.println("java MinecraftController <InfluxDB Server> <DB Name> <DB Measurement> <DB Username> <ModbusTCP Server> <ModbusTCP port> [-modbus4j] [-keepAlive]");
        System.out.println("where:");
        System.out.println("- <InfluxDB Server>:  The complete URL of the InfluxDB server, including the protocol (http or https)...");
        System.out.println("                      Example: https://influx.sdi.hevs.ch");
        System.out.println("- <DB Name>:          The name of the Influx DB to use. For this project, this name is the name of the group you've been affected to. (SInXX)");
        System.out.println("- <DB Username:       The user's name to use to access the DB. It's also your group's name. (SInXX)");
        System.out.println("- <ModbusTCP Server>: The IP address of the Minecraft ModbusTCP server (default value: localhost)");
        System.out.println("- <ModbusTCP port>:   The port number of the Minecraft ModbusTCP server (default value: 1502)");
        System.out.println("- [-eraseDB]:         Optional parameter! If set, the application will erase the previous data in InfluxDB...");
        System.out.println();
        System.exit(1);
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {

        // ------------------------------------- DO NOT CHANGE THE FOLLOWING LINES -------------------------------------
        String dbProtocol = "http";
        String dbHostName = "localhost";
        String dbOrganisation = "labo";
        String dbBucket = "root";
        String dbToken = null;

        String modbusTcpHost = "localhost";
        int modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length >= 6) {
            parameters = args;

            // Decode parameters for influxDB
            String[] dbParams = parameters[0].split("://");
            if (dbParams.length != 2) {
                usage();
            }

            dbProtocol = dbParams[0];
            dbHostName = dbParams[1];
            dbOrganisation = parameters[1];
            dbBucket = parameters[2];
            dbToken = parameters[3];

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[4];
            modbusTcpPort = Integer.parseInt(parameters[5]);

            for (int i = 5; i < args.length; i++) {
                if (parameters[i].compareToIgnoreCase("-erasedb") == 0) {
                    ERASE_PREVIOUS_DATA_INB_DB = true;
                }
            }
        } else {
            usage();
        }

        // ------------------------------------ /DO NOT CHANGE THE FOLLOWING LINES -------------------------------------
        // Start coding here ...

        /**
         * la méthode est applée pour chargé un fichier de configuration pour les point de données ModbusTCP
          */
        FieldConfigurator.fromCSV("ModbusMap.csv");

        // Configuration's section
        /**
         * La méthode est applé pour configurer les paramètre de connexion ImfluxDB
         */
        DatabaseConnector.getInstance().configure(dbProtocol + "://" + dbHostName, dbOrganisation, dbBucket, dbToken);

        // Test DataBase
//        FloatDataPoint dp = (FloatDataPoint) DataPoint.getDataPointFromLabel("GRID_U_FLOAT");
//        dp.setValue(0.5f);

        // Modbus Polling
        /**
         * Le code tente de se connecter au serveur ModbusTCP à l'aide de la méthode `ModbusAccessor.getInstance().connect`,
         * en transmettant les paramètres de connexion ModbusTCP précédemment analysés.
         * Si la connexion est réussie, il commence à interroger le serveur ModbusTCP toutes les 2000 millisecondes
         * en appelant `ModbusRegister.startPolling`.
         */
        if (ModbusAccessor.getInstance().connect(modbusTcpHost, modbusTcpPort, (byte) 1)) {
            ModbusRegister.startPolling(2000);
        }
        /**
        * initialisation du serveur web
         */
        WebConnector.getInstance();
        // Creation de l'objet smart control
        Controller cl = new Controller();
        cl.startControl(500);
    }

    
}
