package ch.hevs.isi.SmartControl;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.TimerTask;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief c'est une hétitage de la classe TimerTask, cette classe permet la régulation de notre monde
 */
public class ControlTask extends TimerTask {
    FloatDataPoint GRID_U_FLOAT;
    FloatDataPoint BATT_P_FLOAT;
    FloatDataPoint BATT_CHRG_FLOAT;
    FloatDataPoint SOLAR_P_FLOAT;
    FloatDataPoint WIND_P_FLOAT;
    FloatDataPoint COAL_P_FLOAT;
    FloatDataPoint HOME_P_FLOAT;
    FloatDataPoint PUBLIC_P_FLOAT;
    FloatDataPoint FACTORY_P_FLOAT;
    FloatDataPoint BUNKER_P_FLOAT;
    FloatDataPoint WIND_FLOAT;
    FloatDataPoint WEATHER_FLOAT;
    FloatDataPoint WEATHER_FORECAST_FLOAT;
    FloatDataPoint WEATHER_COUNTDOWN_FLOAT;
    FloatDataPoint CLOCK_FLOAT;
    FloatDataPoint REMOTE_COAL_SP;
    FloatDataPoint REMOTE_FACTORY_SP;
    FloatDataPoint COAL_AMOUNT;

    BooleanDataPoint REMOTE_SOLAR_SW;
    BooleanDataPoint REMOTE_WIND_SW;
    private int mode = 0;

    /**
     * Constructeur de la classe TimeManager, où l’heure de début du jeu est calculée à partir d’aujourd’hui à minuit
     * moins le nombre de jours donné
     *
     */
    public ControlTask() {
        BATT_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_P_FLOAT");
        GRID_U_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("GRID_U_FLOAT");
        BATT_CHRG_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        SOLAR_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
        WIND_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("WIND_P_FLOAT");
        COAL_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_P_FLOAT");
        HOME_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("HOME_P_FLOAT");
        PUBLIC_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
        FACTORY_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");
        BUNKER_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
        WIND_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("WIND_FLOAT");
        WEATHER_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("WEATHER_FLOAT");
        WEATHER_FORECAST_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel(" WEATHER_FORECAST_FLOAT");
        WEATHER_COUNTDOWN_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel(" WEATHER_COUNTDOWN_FLOAT");
        CLOCK_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel(" CLOCK_FLOAT");
        REMOTE_COAL_SP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        REMOTE_FACTORY_SP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        COAL_AMOUNT = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_AMOUNT");
        REMOTE_SOLAR_SW = (BooleanDataPoint) DataPoint.getDataPointFromLabel(" REMOTE_SOLAR_SW");
        REMOTE_WIND_SW = (BooleanDataPoint) DataPoint.getDataPointFromLabel(" REMOTE_SOLAR_SW");

    }

    /**
     * Cette méthode permet d'effectuer une regulation avec les données entrantes et sortantes.
     * Elle effectue une mise en fonction ou un arrêt de la production
     */
    public void controlloop() {
        float productionWatt = WIND_FLOAT.getValue() + SOLAR_P_FLOAT.getValue() + COAL_P_FLOAT.getValue();
        float consumptionWatt = BUNKER_P_FLOAT.getValue() + HOME_P_FLOAT.getValue() + PUBLIC_P_FLOAT.getValue();


        switch (mode) {
            //Normal mode
            case 0:
                REMOTE_COAL_SP.setValue(0);
                if (BATT_CHRG_FLOAT.getValue() <= 0.6f) {
                    mode = 1;
                } else if (BATT_CHRG_FLOAT.getValue() >= 0.80f) {
                    mode = 2;

                } else {
                    if (consumptionWatt < productionWatt) {
                        REMOTE_FACTORY_SP.setValue(0.2f);
                    } else {
                        REMOTE_FACTORY_SP.setValue(0);
                    }
                }
                break;
            case 1: // Batterie trop basse


                if (BATT_CHRG_FLOAT.getValue() <= 0.6f) {
                    REMOTE_FACTORY_SP.setValue(0);

                    if (productionWatt <= consumptionWatt && COAL_AMOUNT.getValue() >= 0.7f) {

                        REMOTE_COAL_SP.setValue(1);

                    }
                } else {
                    mode = 0;

                }
                break;


            case 2: //Batterie trop haute


                if (BATT_CHRG_FLOAT.getValue() >= 0.8f) {
                    REMOTE_COAL_SP.setValue(0);
                    REMOTE_FACTORY_SP.setValue(1f);



                } else {
                    mode = 0;
                }
                break;
        }

    }

    /**
     * La regulation est applée à chaque fois que la ControlTask est éxecutée cela maintient la production et le consomation
     */
    @Override
    public void run() {
        controlloop();

    }


}