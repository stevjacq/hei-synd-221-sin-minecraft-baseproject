package ch.hevs.isi.SmartControl;
import java.util.Timer;
import ch.hevs.isi.field.ModbusRegister;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief cette classe  permet de contrôler périodiquement, en utilisant la classe Timer de Java.
 */


public class Controller {
    /**
     *permet de de controller periodiquement en utilisant un objet timer
     * @param millis le temps entre chaque exécution ( en milliseconde)
     */
    public static void startControl(int millis) {

        Timer smartControlTimer = new Timer();

        smartControlTimer.scheduleAtFixedRate(new ControlTask(), 0, millis);
    }

}