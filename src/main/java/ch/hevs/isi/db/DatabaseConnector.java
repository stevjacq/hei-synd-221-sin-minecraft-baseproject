package ch.hevs.isi.db;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe implémente  l'interface DataPointListener.
 * ce qui signifie qu'elle écoute les mises à jour des valeurs des DataPoint
 * c'est un "singletons" car  elle a une seule instance
 * C est une classe qui permet de connecter une application à une base de données InfluxDB,
 * de configurer le lien avec la base de données et de pousser les nouvelles valeurs dans la base de données.
 */
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DatabaseConnector implements DataPointListener {
    private static DatabaseConnector instance = null;

    private URL url;
    private String token;
    private TimeManager tm = new TimeManager(3);
    private long timestamp;

    /**
     * cette fonction permet:
     * si le label dp est egale a la valeur du clock
     * -  prend l’index de l’horloge Electrical-Age et génère l’horodatage correspondant.
     * - Donne l’horodatage réel en fonction de l’horloge EA.
     * Si le timestamp egal 0
     * - les valeures sont push sur le database
     * @param dp est un dataPoint
     */
    @Override
    public void onNewValue(DataPoint dp) {

        if (dp.getLabel().equals("CLOCK_FLOAT")) {
            tm.setTimestamp(dp.toString());
            timestamp = tm.getNanosForDB();
                    }
        if(timestamp!=0) {
            pushToDatabase(dp.getLabel(), dp.toString(),timestamp);
        }

    }

    /**
     * cette fonction permet de se connecter a influxdb et
     * que les nouvelles valeurs soient poussées vers la base de donnée en utilisant HTTP POST
     * @param label nom du registre
     * @param value valeur
     * @param timestamp temps
     */
    private void pushToDatabase(String label, String value,long timestamp) {
        try {
            HttpURLConnection connexion = (HttpURLConnection) url.openConnection();
            connexion.setRequestProperty("Authorization", "Token " + token);
            connexion.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            connexion.setRequestMethod("POST");
            connexion.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connexion.getOutputStream());
            writer.write("Minecraft " + label + "=" + value+" "+timestamp);
            writer.flush();
            int responseCode = connexion.getResponseCode();
            System.out.println(responseCode);

        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  cette méthode est une méthode de création de singleton. Si une instance n'existe pas,
     *  elle crée une nouvelle instance de la classe DatabaseConnector et la renvoie.
     * @return l'instance
     */
    public static DatabaseConnector getInstance() {
        if (instance == null) {
            instance = new DatabaseConnector();

        }
        return instance;

    }

    /**
     * Constructeur de la classe
     */
    private DatabaseConnector() {
    }

    /**
     * cette méthode configure le lien vers la base de données en utilisant l'URL de base,
     * l'organisation, le bucket et le token.
     * @param baseURL
     * @param organization  chaque groupe a son organisation
     * @param bucket le lieu de stocage des mesures
     * @param token le mots de passe
     */
    public void configure(String baseURL, String organization, String bucket, String token) {
        this.token = token;
        try {
            this.url = new URL(baseURL + "/api/v2/write?org=" + organization + "&bucket=" + bucket);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


}

