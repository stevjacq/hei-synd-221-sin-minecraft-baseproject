package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe est un heritage de la classe DataPoint
 *
 */
public class FloatDataPoint extends DataPoint {
    private float value;

    /**
     * ce constructeur  invoque le constructeur de la classe DataPoint
     * @param label du point de données
     * @param isOutput vrais si c'est une sortie , faux si c'est une entrée
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Cette méthode permet de définir une valeur de type float pour le point de données
     * @param value La valeur à définir
     * La méthode pushToConnectors() est héritée de la classe DataPoint et n'a pas besoin d'être redéfinie.
     */
    public void setValue(float value) {
        this.value = value;
        pushToConnectors();
    }

    /**
     * Cette méthode permet de convertir la valeur du point de données en une chaîne de caractères
     * @return La valeur du point de données sous forme de chaîne de caractères
     */
    public String toString() {
        return String.valueOf(value);
    }

    /**
     *cette focntion permet d'acceder à la valeur d'un registre en float
     * @return une valeur
     */
    public Float getValue() {
        return value;
    }
}
