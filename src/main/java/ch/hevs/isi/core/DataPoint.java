package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe est la classe maitre
 *
 * En détail au cœur de la supervision, il y a des objets DataPoint : un
 * DataPoint représente une entrée de champ ou une sortie de champ
 */
public abstract class DataPoint {
    private String label;
    private boolean isOutput;
    private static HashMap<String, DataPoint> dataPointMap = new HashMap<>();

    /**
     * l'objet  forme le jumeau numérique de "field process"
     * il est fait d'une valeur boleanne ou float et d'un indentifiant de chaine applé label
     * Les valeurs doivent être stockées uniquement dans des objets DataPoint
     *  Les objets DataPoint ne doivent pas être liés à une technologie de terrain (comme
     * Modbus)
     * c'est-à-dire que la définition des objets DataPoints ne doit pas être modifiée
     * @param label
     * @param isOutput
     */
    protected DataPoint(String label, boolean isOutput) {
        this.label = label;
        this.isOutput = isOutput;

        dataPointMap.put(label, this);
    }

    /**
     * cette focntion permet d'acceder au datapoint d'un label
     * @param label
     * @return
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return dataPointMap.get(label);
    }

    /**
     * cette fonction permet d'acceder au label
     * @return le label
     */
    public String getLabel() {
        return label;
    }

    /**
     * cette fonction pertmet d'acceder si c'est une sortie
     * @return  vrai ou faux
     */
    public boolean isOutput() {
        return isOutput;
    }

    /**
     * cette  fonction permet de seter une valeur de type boolean
     * @param value
     */
    public void setValue(boolean value) {
        // Default implementation: do nothing
    }

    /**
     * cette fontion est applée par les classes héritiaires
     *
     */
    public abstract Object getValue() ;

    /**
     * cette fonction permet de seter une valeur de type float
     * @param value
     */
    public void setValue(float value) {
        // Default implementation: do nothing
    }

    /**
     * cette fonction est implémentée par les classes dériviées
     * @return
     */
    public abstract String toString();

    /**
     *Cette fonction envoie la valeur du point de données aux connecteurs de base de données,
     * web et terrain (si le point de données est une sortie).
     */
    protected void pushToConnectors() {
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);

        if (isOutput)
            FieldConnector.getInstance().onNewValue(this);
    }

    /**
     * Ici nous testons la focntionnalité du settage de nos valeurs
     * @param args
     */
    public static void main(String[] args) {
        new FloatDataPoint("fdp1", true);
        new BooleanDataPoint("bdp1", false);

        DataPoint fdp = DataPoint.getDataPointFromLabel("fdp1");
        DataPoint bdp = DataPoint.getDataPointFromLabel("bdp1");

        if (fdp != null)
            fdp.setValue(1.5f);

        if (bdp != null)
            bdp.setValue(true);
    }

}
