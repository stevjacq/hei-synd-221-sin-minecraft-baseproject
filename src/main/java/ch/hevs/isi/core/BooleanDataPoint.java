package ch.hevs.isi.core;
/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Cette classe est un héritage de la classe DataPoint
 *
 * En détail nous y settons, demendons et passons en String  des valeurs
 */

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

public class BooleanDataPoint extends DataPoint {
    private boolean value;

    /**
     * Ce constructeur invoque le constructeur de la classe DataPoint
     * @param label
     * @param isOutput
     */
    public BooleanDataPoint(String label, boolean isOutput) {
        super(label, isOutput);


    }


    /**
     * la fonction permet de seter une valeure
     * @param value c'est la valeur qui est seter
     */
    public void setValue(boolean value) {
        this.value=value;
        pushToConnectors();
    }

    /**
     * la fonction permet de convertir une valeur en string
     * @return valeur convertie en string
     */
    @Override
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * la focntion permet d'avoir la valeur d'un objet
     * @return value = valeur de l'objet
     */
    public Boolean getValue(){
        return value;
    }
}
