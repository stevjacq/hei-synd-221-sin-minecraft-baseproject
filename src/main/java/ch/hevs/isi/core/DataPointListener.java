package ch.hevs.isi.core;

/**
 * @author elise Zanou, steven Jacquier
 * @date 21/04/2023
 * @brief Dans cette interface nous y trouvons la fonction onNewValue

 */
public interface DataPointListener {
  public void onNewValue(DataPoint dp);
}
